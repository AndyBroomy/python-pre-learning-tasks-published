def vowel_swapper(string):
    # ==============
    # Your code here
    dict = {'a': '4', 'A': '4', 'e': '3', 'E': '3', 'i': '!', 'I': '!', 'o': 'ooo', 'O': '000', 'u': '|_|', 'U': '|_|'}
    vowel = ['a','e','i','o','u']
    vcount = 0
    second = 0
    list1 = list(string)

    for item in vowel:
        second = string.lower().find(item) 
        vcount = string.lower().find(item, second + 1) 
        if second != vcount and vcount!=-1:
            list1[vcount] = dict[string[vcount]]
    string = "".join(list1)

    return(string)
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
