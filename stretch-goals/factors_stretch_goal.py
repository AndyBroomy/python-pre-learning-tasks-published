def factors(number):
    # ==============
    # Your code here
    a = []
    for x in range(2, number):
        if (number % x) == 0:
            a = a + [x]
    if len(a) == 0:
        return (f'"{number} is a prime number"')
    return (a)
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
