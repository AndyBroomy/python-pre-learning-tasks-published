def calculator(a, b, operator):
    # ==============
    # Your code here
    ans = []
    if operator == '+':
        x = (a + b)
    elif operator == '-':
        x = (a - b)
    elif operator == '*':
        x = (a * b)
    elif operator == '/':
        x =  (a // b)
    while x > 0:
        ans = ans + [str(x%2)]
        x = x // 2
    ans = (ans[::-1]) #reverses list
    ans = ("".join((ans)))
    return ans
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
